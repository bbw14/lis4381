> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile App Development


## Behrens Watkins


### Project 2 Requirements:

1. Add edit and delete funcationality to A5
2. Create a page with an RSS feed
3. Screenshots of project requirements
4. Chapter Questions (Chs 11, 12)
5. Link to local lis4381 web app: http://localhost/lis4381


### README.md file should include the following items:
* Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
* Blackboard Links: lis4381 Bitbucket repo


#### Assignment Screenshots:

*Screenshot of Initial Page*:

![Initial Page](img/initial.png)

*Screenshot of Failed Validation*:

![Failed Validation](img/error.png)

*Screenshot of  Delete*:

![Delete](img/after_delete.png)

*Screenshot of  Edit Page*:

![Edit Page](img/edit.png)

*Screenshot of  Home*:

![Home](img/home.png)

*Screenshot of  RSS*:

![RSS](img/rss.png)

#### Assignment Links:
[http://localhost/](http://localhost/repos/lis4381/p2/index.php)

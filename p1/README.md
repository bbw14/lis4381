> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile App Development


## Behrens Watkins


### Project 1 Requirements:

1. Create a business card app.


### README.md file should include the following items:
* Screenshot of first running screen.
* Screenshot of second screen.

#### Assignment Screenshots:

*Screenshot of First Running Screen*:

![ERD Screenshot](img/one.png)

*Screenshot of Second Running Screen*:

![Second Screenshot](img/two.png)

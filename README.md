
# LIS4381 - Mobile Web Application Development

## Behrens Watkins

### Assignment Requirements

1. [A1 README.md ](a1/README.md "My A1 README.md file")

    * install AMPPS
    * install JDK
    * install Android Studio and create My First App
    * Provide screenshots of installations
    * create bitbucket repo
    * complete bitbucket Tutorials
    * provide git command descriptions

2. [A2 README.md ](a2/README.md "My A2 README.md file")

    * develop a user interface using the TextView, ImageView, and Button controls
    * add text in strings.xml using the Translations Editor
    * Add a Java class file
    * write code using the onCreate method
  use the OnClickListener to detect user interactions
    * launch a second screen using a startActivity method


3. [A3 README.md ](a3/README.md "My A3 README.md file")

    * Screenshot of ERD;
    * Screenshot of running applications's first user interface;
    * Screenshot of running applications second user interface;
    * links to the following files
        * a. a3.mbw
        * b. a3.sql

4. [P1 README.md ](p1/README.md "My P1 README.md file")

    * Screenshot of First Running Screen;
    * Screenshot of Second Running Screen;

5. [A4 README.md ](a4/README.md "My A4 README.md file")

    * Client-side validation
    * Link to localhost
    * Screenshot of main portal page
    * Screenshot of passed and failed validation

6. [A5 README.md ](a5/README.md "My A5 README.md file")
    * Server-side validation
    * Link to localhost
    * Screenshot of index.php
    * Screenshot of app_petstore_process.php

7. [P2 README.md ](p2/README.md "My P2 README.md file")
    * Add edit functionality to A5
    * Add delete functionality to A5
    * Create an RSS feed page

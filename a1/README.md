> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course Title
LIS4381 - Mobile Web Application Development
## Your Name
Behrens Watkins
### Assignment # Requirements:
A1
*Sub-Heading:*

1. distributed version control with git and bitbucket
2. development Installations
3. chapter questions 1,2

#### README.md file should include the following items:

* Screenshots of AMPPS installation
* Screenshot of running java Hello
* Screenshot of running android Studio - My First App
* Git commands with short descriptions
* Bitbucket repo Links a) this Assignment and b) the completed Tutorials

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init -- creates a new repo
2. git status -- shows files changed and shows files you need to add or commit
3. git add -- add files to staging area
4. git commit -- commits files added with git add
5. git push -- send changes to master branch of remote repo
6. git pull -- fetch and merge changes on the remote server to working directory
7. git push --all origin -- push all branches to repo

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:
![AMPPS Running](img/localhost1.png)
![AMPPS Running 2](img/localhost2.png)


![AMPPS Installation Screenshot](img/AMPPS.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/app.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bbw14/bitbucketstationlocations/src/aa036b86f9aa?at=master)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/bbw14/myteamquotes)



#### Questions

1. You create line comments in PHP code by adding to a line you want to use as a comment. (Choose all that apply.)
d. //
c. #

2. Block comments begin with /* and end with _____.
b. * /

3. PHP variables begin with _____________________.
c. $

4. A variable name in PHP can only contain letters, numbers,
and _______________________.
a. underscores

5. A PHP variable name
a. is case-sensitive

6. A web application is a type of
a. client/server application

7. After the if statement that follows is executed, what will the value of $discountAmount be?
$discount_amount;
$order_total = 200;
if ($order_total > 200) {
  $discount_amount = $order_total * .3;
} else if ($order_total > 100) {
  $discount_amount = $order_total * .2;
} else {
  $discount_amount = $order_total * .1;
}
c. 40

8. How many times will the while loop that follows be executed?
$months = 5;
$i = 1;
while ($i > $months) {
 $futureValue = $futureValue * (1 + $monthlyInterestRate);
 $i = $i+1;
}
a. 0

9. If a URL specifies a directory that doesn’t contain a default page, Apache displays
d. a list of all of the directories in that directory

10. In the code that follows, if $error_message isn’t empty
 if ($error_message != '') {
 include('index.php');
 exit();
 }
a. control is passed to a page named index.php that’s in the current directory

11. The HTTP response for a dynamic web page is passed
d. from the web server to the browser

12. The order of precedence for arithmetic expressions causes
c. increment operations to be done first

13. To round and format the value of a variable named $number to 3 decimal places and store it in a variable named $number_formatted, you code
b. $number_formatted = number_format($number,3);

14. To run a PHP application that has been deployed on your own computer, you can enter a URL in the address bar of your browser that
b. uses localhost as the domain name

15. To view the source code for a web page in the Firefox or IE browser, you can select the appropriate command from the
b. Source menu

16. When the web server receives an HTTP request for a PHP page, the web server calls the
d. PHP interpreter

17. Which of the following is NOT part of an HTTP URL:
c. node

18. What will the variable $name contain after the code that follows is executed?
$first_name = 'Bob';
$last_name = 'Roberts';
$name = "Name: $first_name";
c. Name: Bob

19. In PHP, the concatenation operator is
c. .

20. In a conditional expression that includes logical operators, the AND operator has a lower order of precedence than the ______________ operator.
 a. NOT

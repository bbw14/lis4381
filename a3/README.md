> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile App Development


## Behrens Watkins


### Assignment 3 Requirements:

1. Create a database diagram.
2. Create a mobile application.
3. Chapter Questions


### README.md file should include the following items:
* Screenshot of ERD;
* Screenshot of running applications's first user interface;
* Screenshot of running applications second user interface;
* links to the following files
    * a. a3.mbw
    * b. a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/ERD.png)

*Screenshot of Running First Screen*:

![Second Screenshot](img/first.png)

*Screenshot of Running Second Screen*:

![Second Screenshot](img/second.png)

#### Assignment Links:

* [a3.sql](docs/a3.sql)
* [a3.mwb](docs/a3.mwb)

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile App Developement


## Behrens Watkins


### Assignment 2 Requirements:

1. Create the user interface, also called an XML layout, for every screen in the application.
2. Create a Java class, also called an Activity, for every screen in the application.
3. Code each Java class with the appropriate objects and actions as needed.
4. Test the application in the Android emulator.


### README.md file should include the following items:
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of Home screen*:

![Home Screenshot](img/home.png)

*Screenshot of Second Screen*:

![Second Screenshot](img/click.png)

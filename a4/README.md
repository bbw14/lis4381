> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile App Development


## Behrens Watkins


### Assignment 4 Requirements:

1. Course title, your name, assignment requirements
2. Screenshots of client-side valiadation
3. Link to local lis4381 web app:
[http://localhost/](http://localhost/repos/lis4381/a4/index.php)


### README.md file should include the following items:
* Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
* Blackboard Links: lis4381 Bitbucket repo
* Note: the carousel must contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills.

#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page](img/main.png)

*Screenshot of Failed Validation*:

![Failed Validation](img/fail.png)

*Screenshot of Passed Validation*:

![Pass Validation](img/pass.png)

#### Assignment Links:
[http://localhost/](http://localhost/repos/lis4381/a4/index.php)
